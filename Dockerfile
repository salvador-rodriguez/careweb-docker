# Version: 0.0.1

FROM ubuntu:16.04
MAINTAINER Salvador Rodriguez <salvador.rodriguez@utah.edu>

# Install packages
ENV REFRESHED_AT 2016-09-28
RUN apt-get update && \
	apt-get install -yq --no-install-recommends wget unzip tar pwgen ca-certificates && \
	apt-get clean && \
	rm -rf /var/lib/apt/lists/*

# Install Java
RUN wget --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u101-b13/jdk-8u101-linux-x64.tar.gz
RUN mkdir /usr/local/java
RUN tar -zxf jdk-8u101-linux-x64.tar.gz -C /usr/local/java
RUN rm -f jdk-8u101-linux-x64.tar.gz

ENV JAVA_HOME /usr/local/java/jdk1.8.0_101
RUN echo PATH=$JAVA_HOME/bin:$PATH:$HOME/bin >> ~/.bashrc
RUN echo export PATH JAVA_HOME >> ~/.bashrc

# Install TOMCAT
ENV TOMCAT_MAJOR_VERSION 9
ENV TOMCAT_MINOR_VERSION 9.0.0.M9
ENV CATALINA_HOME /usr/local/tomcat

RUN wget -q https://archive.apache.org/dist/tomcat/tomcat-${TOMCAT_MAJOR_VERSION}/v${TOMCAT_MINOR_VERSION}/bin/apache-tomcat-${TOMCAT_MINOR_VERSION}.tar.gz && \
    wget -qO- https://archive.apache.org/dist/tomcat/tomcat-${TOMCAT_MAJOR_VERSION}/v${TOMCAT_MINOR_VERSION}/bin/apache-tomcat-${TOMCAT_MINOR_VERSION}.tar.gz.md5 | md5sum -c - && \
    tar zxf apache-tomcat-*.tar.gz && \
    rm apache-tomcat-*.tar.gz && \
    mv apache-tomcat* ${CATALINA_HOME}

COPY files/tomcat/tomcat-users.xml ${CATALINA_HOME}/conf/tomcat-users.xml
COPY files/tomcat/setenv.sh ${CATALINA_HOME}/bin/setenv.sh

# Install careweb
COPY files/careweb/cwfdemo-webapp-connectathon-1.0.0-SNAPSHOT.war /
RUN unzip cwfdemo-webapp-connectathon-1.0.0-SNAPSHOT.war -d ${CATALINA_HOME}/webapps/cwfdemo-webapp
RUN rm -f cwfdemo-webapp-connectathon-1.0.0-SNAPSHOT.war
COPY files/careweb/bilirubin_chart.smart ${CATALINA_HOME}/webapps/cwfdemo-webapp/WEB-INF/
COPY files/careweb/cwf.properties ${CATALINA_HOME}/webapps/cwfdemo-webapp/WEB-INF/classes/

# Add VOLUMEs
VOLUME ["/root/.cwf", "/usr/local/tomcat/webapps"]

EXPOSE 8080

CMD ["/usr/local/tomcat/bin/catalina.sh", "run"]

