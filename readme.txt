Build image 
docker build -t="opencds/careweb" .

Create container 
docker run -p 8081:8080 -d --name careweb opencds/careweb
docker run -it -p 8081:8080 --name careweb opencds/careweb /bin/bash

Open browser on
- http://localhost:8080/cwfdemo-webapp

user/pass demo/demo
